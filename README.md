
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Ejercicio 2:

 /**
  * Dado un endpoint ENDPOINT
  * y una funcion 'fetch' que hace un request get y devuelve una promesa Promise<data>
  * implementar un hook useFetch que permita saber:
  *   data: Object
  *   error: Object
  *   loading: boolean
  */

// Ejemplo de uso:
const { data, loading, error } = useFetch(ENDPOINT);
