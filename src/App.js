import React from "react";
import "./App.css";
import { useFetch } from "./hooks";

function App() {
  const { response, isLoading, error } = useFetch(
    "https://dog.ceo/api/breeds/image/random"
  );

  const showTitle = () => {
    if (isLoading) {
      return <h1>Loading...</h1>;
    } else if (error) {
      return <h1>Error</h1>;
    } else {
      return <h1>{status}</h1>;
    }
  };

  const showImage = () => {
    if (!isLoading) {
      return <img src={imageUrl} alt="image" />;
    }
  };

  const status = response.status;
  const imageUrl = response.message;

  return (
    <div className="App">
      <div>
        {showTitle()}
        <div>{showImage()}</div>
      </div>
    </div>
  );
}

export default App;
